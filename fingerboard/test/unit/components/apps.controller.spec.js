describe('AppsController controller', function() {
    var scope;
    var ctrl;

    var singleAppInvalid = {
        name: "agent",
        on: true, 
        headBandName: "Bandeau agent",
        url: "http://www.googe.fr",
        response: {header: "OK", status: "200"}
    };
    beforeEach(angular.mock.module('apps.controller'));

    beforeEach(angular.mock.module('apps.services'));
  
    beforeEach(angular.mock.inject(function($rootScope, $controller, _$window_, _AppsServices_) {
      scope = $rootScope.$new();
      ctrl = $controller('AppsController', { '$scope': scope, '$window': _$window_, 'AppsServices': _AppsServices_ });
    }));
  
    it('can instantiate controller', function(){
      expect(ctrl).not.toBeUndefined();
      expect(scope.getActiveImage({app: {name: 'test'}})).not.toBeUndefined();
    });
    it('can hover image', function(){
        expect(scope.getActiveImage(singleAppInvalid)).toBe('../assets/images/agent.svg')
        expect(scope.getHoverImage(singleAppInvalid)).toBe('../assets/images/agent-hover.svg')
        expect(scope.getLightImage(singleAppInvalid)).toBe('../assets/images/green-light.svg')
    });
    it('can get status, header and headBandName', function(){
        expect(scope.getStatus(singleAppInvalid)).toBe('200')
        expect(scope.getHeader(singleAppInvalid)).toBe('OK')
    });
    it( 'can call window.open', inject( function( $window ) {
        spyOn( $window, 'open' ).and.callFake( function() {
            return true;
        } );
        scope.open(singleAppInvalid);
        expect( $window.open ).toHaveBeenCalled();
        expect( $window.open ).toHaveBeenCalledWith( "http://www.googe.fr","_self" );
    }));
  });