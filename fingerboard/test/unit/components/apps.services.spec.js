describe('AppsServices factory', function() {
    var AppsServices, $q, $httpBackend, $window;
   
    var singleAppValid = {
        name: "agent",
        on: true, 
        headBandName: "Bandeau agent",
        url: "https://fr.wikipedia.org/wiki/Sigur_Rós",
        response: {header: "OK", status: "200"}
    };

    var singleAppInvalid = {
        name: "agent",
        on: true, 
        headBandName: "Bandeau agent",
        url: "http://www.googe.fr",
        response: {header: "OK", status: "200"}
    };

    var RESPONSE_ERROR = {
        'detail': 'Not found.'
    };
  
    beforeEach(angular.mock.module('apps.services'));
   
    beforeEach(angular.mock.inject(function(_AppsServices_,_$q_,_$httpBackend_,_$window_) {
      AppsServices = _AppsServices_;
      $q = _$q_;      
      $httpBackend = _$httpBackend_;     
      $window = _$window_;
    }));
    
    it('should exist', function() {
        expect(AppsServices).toBeDefined();
      });
  
    describe('.getAppList()', function() {
        
        it('should exist', function() {
            expect(AppsServices.getAppList).toBeDefined();
        });
        it('should exist', function() {
            expect(AppsServices.getAppList).toBeDefined();
        });

    });

    describe('.updateOn()', function() {
        
        beforeEach(function() {
            result = {};
            spyOn(AppsServices, "updateOn").and.callThrough();
            singleAppInvalid.on = true;
            singleAppValid.on = true;

          });
        
        it('should exist', function() {
            expect(AppsServices.updateOn).toBeDefined();
        });

        it('should return an app when its url is valid', function() {
            
            $httpBackend.expectGET(singleAppValid.url).respond(200,$q.when(singleAppValid));

            AppsServices.updateOn(singleAppValid)
            .then(function(res){
                result = res;
            });

            $httpBackend.flush();
            
            expect(AppsServices.updateOn).toHaveBeenCalledWith(singleAppValid);
            expect(result.headBandName).toEqual("Bandeau agent");
            expect(result.on).toEqual(true);
            expect(result.url).toEqual("https://fr.wikipedia.org/wiki/Sigur_Rós");
        })
        
        it('should return a 404 when its url is invalid', function() {
            
            $httpBackend.expectGET(singleAppInvalid.url).respond(404,Common.silenceUncaughtInPromise($q.reject(RESPONSE_ERROR)));
            
            AppsServices.updateOn(singleAppInvalid)
            .catch(function(res){
                result = res;
            });

            $httpBackend.flush();
            
            expect(AppsServices.updateOn).toHaveBeenCalledWith(singleAppInvalid);
        })
        it('should off the app when its url is invalid', function() {
            
            expect(singleAppInvalid.on).toEqual(true);
            
            $httpBackend.expectGET(singleAppInvalid.url).respond(404,Common.silenceUncaughtInPromise($q.reject(RESPONSE_ERROR)));
            
            AppsServices.updateOn(singleAppInvalid)
            .catch(function(res){
                result = res;
            });

            $httpBackend.flush();
            
            expect(AppsServices.updateOn).toHaveBeenCalledWith(singleAppInvalid);
            expect(singleAppInvalid.on).toEqual(false);
        })
        it('should let the app on when its url is valid', function() {
            
            expect(singleAppValid.on).toEqual(true);

            $httpBackend.expectGET(singleAppValid.url).respond(200,$q.when(singleAppValid));
            
            AppsServices.updateOn(singleAppValid)
            .catch(function(res){
                result = res;
            });

            $httpBackend.flush();
            
            expect(AppsServices.updateOn).toHaveBeenCalledWith(singleAppValid);
            expect(singleAppValid.on).toEqual(true);
        })
    })
})