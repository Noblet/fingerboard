var express = require('express');
var path = require('path');
var app = express();

var publicPath = path.resolve(__dirname, 'webdist');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(8080);