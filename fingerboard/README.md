Installation
============

* Get the dependencies and check tests
  ```
  npm install
  npm test
  npm run build
  ```

* Update connexion parameters in app/config/config.json

    

* Start the server

  ```
  npm start
  ```

Usage
=====

Files are available in app/webdist/, you may copy the entire directory to your web server

Manual Installation
===================

* Add fingerboard directory to xivocc nginx in yml file

  ```yaml
    nginx:
      image: xivoxc/xivoxc_nginx:latest

      ports:
      - 8080:8080

      volumes:
      - /usr/share/fingerboard

      restart: always


Deploy Using Docker
===================

Build docker image :

* update release.txt
* and version.txt
* and start ./build.sh

License
=======

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
