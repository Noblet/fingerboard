const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const HtmlWebPackPlugin = require('html-webpack-plugin');

const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {

  context: __dirname + '/app',
  
  entry: {
    app: ['./components/apps.services.js','./components/apps.controller.js','./components/apps.directive.js'],
    vendor: ['angular','angular-ui-bootstrap','./app.js']
  },

  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'webdist')
  },
  
  plugins: [
    new HtmlWebPackPlugin({
      title: "XIVO CC",
      filename: "index.html",
      template: "index.html",
      favicon: "assets/images/favicon.ico",
    }),
    
    new HtmlWebPackPlugin({
      filename: "./appsList.html",
      template: "./components/appsList.html",
      inject: false
    }),
    new StyleExtHtmlWebpackPlugin({
      minify: true
    }),
    extractCSS = new ExtractTextPlugin({
      filename: "[name].css"
    }),
    new CopyWebpackPlugin([
      {from: 'assets', to: 'assets' }
    ]),
    new CleanWebpackPlugin("webdist")
  ],
   
  devServer: {
     headers: { "Access-Control-Allow-Origin": "*" }
  },
  node: {
    fs: "empty",
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },
  module: {
    rules: [ 
      {
        test: /\.(ico|png|jpg|ttf)$/,
        use:{
          loader: 'file-loader?name=[path][name].[ext]',
        }
      },  
      {
        test:/\.css$/, 
        use: ExtractTextPlugin.extract({ 
                fallback:'style-loader',
                use:'css-loader',
            })
      },  
  ]
}
};
