import '../assets/css/xivoxc.css';

angular.module('apps.directive',[])
.directive('appsList', function() {
    return {          
        templateUrl: "appsList.html"
    };
});