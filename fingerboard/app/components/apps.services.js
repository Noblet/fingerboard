angular.module('apps.services', [])
.factory('AppsServices', function($window, $http) {

    var Apps = {};

    Apps.appList = [
    {
        name: "agent",
        on: true, 
        headBandName: "Bandeau agent",
        url: "http://" + $window.location.hostname + ":8070/ccagent",
        response: {header: "OK", status: "200"}
    },
    {
        name: "manager",
        on: true,   
        headBandName: "CC Manager",
        url: "http://" + $window.location.hostname + ":8070/ccmanager",
        response: {header: "OK", status: "200"}
    },
    {
        name: "recordings",
        on: true,  
        headBandName: "Enregistrements",
        url: "http://" + $window.location.hostname + ":9400",
        response: {header: "OK", status: "200"}
    },
    {
        name: "configuration",
        on: true,   
        headBandName: "Configuration",
        url: "http://" + $window.location.hostname + ":9100",
        response: {header: "OK", status: "200"}
    },    
    {
        name: "spagobi",
        on: true,   
        headBandName: "Statistiques",
        url: "http://" + $window.location.hostname + ":9500/SpagoBI",
        response: {header: "OK", status: "200"}
    },
    {
        name: "kibana",
        on: true, 
        headBandName: "Monitoring",
        url: "http://" + $window.location.hostname + "/kibana",
        response: {header: "OK", status: "200"}
    }
    ];
    
    Apps.getAppList = function() {
        return Apps.appList;
    };
    Apps.updateAllOn = function() {
        for (app in Apps.appList) {
           Apps.updateOn(Apps.appList[app]);
	    }
    }
    Apps.updateOn = function(app){
        
        return $http.get(app.url)
        .then(
            function(res) {
                console.log(res);
                
                app.on = true;
                app.response.header = "OK"
                app.response.status = res.status                
                return res.data;
            },
            function(res) {
                console.log(res);
                app.on = false;
                app.response.header = "ERROR"  
                app.response.status = res.status                              
                return res.data;
            }
        )
    }
    return Apps;
});
