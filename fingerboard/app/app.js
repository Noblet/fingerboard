angular.module('app', [
  'ui.bootstrap',
  'apps.services',
  'apps.controller',
  'apps.directive'
])